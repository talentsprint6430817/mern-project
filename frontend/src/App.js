import React, { useState, useContext} from "react";
import { Routes, Route } from "react-router-dom";


import './App.css'
import ApplicationRouting from "./pages/ApplicationPages/ApplicationRouting";
import LandingRouting from "./pages/LandingPages/LandingRouting";
import { AuthContext } from './AuthProvider';


function App() {
  const { isLoggedIn, setIsLoggedIn } = useContext(AuthContext);
  return (
    <>
    {/* <ApplicationRouting /> */}
    {isLoggedIn ? <ApplicationRouting /> : <LandingRouting />}
    </>
    
  );
}

export default App;
