import React, { useState } from "react";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPlus, faXmark } from "@fortawesome/free-solid-svg-icons";
import "../styles/Application.css";

export default function TagInput({ tags, setTags }) {
  const [inputValue, setInputValue] = useState("");

  const handleInputChange = (e) => {
    setInputValue(e.target.value);
  };

  const addNewTag = () => {
    if (inputValue.trim() !== "") {
      setTags([...tags, inputValue.trim()]);
      setInputValue("");
    }
  };

  const handleKeyDown = (e) => {
    if (e.key === "Enter") {
      addNewTag();
    }
  };

  const handleRemoveTag = (tagToRemove) => {
    setTags(tags.filter((tag) => tag !== tagToRemove));
  };

  return (
    <div>
      {tags?.length > 0 && (
        <div className="tags-list">
          {tags.map((tag, index) => (
            <span key={index} className="tags">
              # {tag}
              <sup>
                <button
                  onClick={() => {
                    handleRemoveTag(tag);
                  }}
                  className="tag-remove"
                >
                  <FontAwesomeIcon icon={faXmark} className="close-icon" />
                </button>
              </sup>
            </span>
          ))}
        </div>
      )}

      <div className="tag-input-container">
        <input
          type="text"
          value={inputValue}
          className="tag-input"
          placeholder="Add tags"
          onChange={handleInputChange}
          onKeyDown={handleKeyDown}
        />

        <div>
          <FontAwesomeIcon icon={faPlus} className="tag-add-icon"   onClick={() => {
            addNewTag();
          }}/>
        </div>
      </div>
    </div>
  );
}
