import React from "react";
import { Form, FormGroup, Input } from "reactstrap";

export default function FilterBy({ getAllNotes, getPinned }) {
  const handleFilterChange = (event) => {
    if (event.target.value === "all") {
      getAllNotes();
    } else if (event.target.value === "pinned") {
      getPinned();
    }
  };

  return (
    <div className="filter-container">
      <Form>
        <FormGroup>
          <Input
            id="exampleSelect"
            name="select"
            type="select"
            onChange={handleFilterChange}
          >
            <option value="all">All notes</option>
            <option value="pinned">Pinned Notes</option>
          </Input>
        </FormGroup>
      </Form>
    </div>
  );
}
