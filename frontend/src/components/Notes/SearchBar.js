import React from "react";

// * Importing icons
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faMagnifyingGlass, faXmark } from "@fortawesome/free-solid-svg-icons";

export default function SearchBar({
  value,
  onChange,
  handleSearch,
  onClearSearch,
}) {
  return (
    <div className="searchbar-container">
      <input
        type="text"
        placeholder="Search Notes"
        className="searchbar"
        value={value}
        onChange={onChange}
      />
      <div className="search-icons-container">
        {value && (
          <FontAwesomeIcon
            icon={faXmark}
            className="search-icon"
            onClick={onClearSearch}
          />
        )}

        <FontAwesomeIcon
          icon={faMagnifyingGlass}
          className="search-icon"
          onClick={handleSearch}
        />
      </div>
    </div>
  );
}
