import React, { useState, useEffect } from "react";
import axios from "axios";

// * Importing components
import TagInput from "../TagInput";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faXmark } from "@fortawesome/free-solid-svg-icons";
import axiosInstance from "../../utils/axiosInstance";

// * Importing urls


export default function AddEditNote({ type, noteData, getAllNotes, onClose, showToastMessage }) {
  const [title, setTitle] = useState(noteData?.title || "");
  const [content, setContent] = useState(noteData?.content || "");
  const [tags, setTags] = useState(noteData?.tags || []);
  const [error, setError] = useState(null);

  // Add note
  const addNewNote = async () => {
    try {
      const response = await axiosInstance.post('/notes', {
        title,
        content,
        tags,
      });
      if (response.data) {
        showToastMessage("Note Successfully", "add")
        getAllNotes();
        onClose();
      }
    } catch (error) {
      if (error.response && error.response.data && error.response.data.message) {
        setError(error.response.data.message);
      }
    }
  };

  // Edit Note
  const editNote = async () => {
    try {
      const response = await axiosInstance.put('/notes/'+ noteData._id, {
        title,
        content,
        tags,
      });
      if (response.data) {
        showToastMessage("Updated Successfully", "edit")
        getAllNotes();
        onClose();
      }
    } catch (error) {
      if (error.response && error.response.data && error.response.data.message) {
        setError(error.response.data.message);
      }
    }
  };

  const handleAddNote = () => {
    if (!title) {
      setError("Please enter the title");
      return;
    }
    if (!content) {
      setError("Please enter the content");
      return;
    }
    setError("");
    if (type === "edit") {
      editNote();
    } else {
      addNewNote();
    }
  };

  useEffect(() => {
    getAllNotes();
  }, []);

  return (
    <div className="modal-container">
      <button className="modal-close-btn" onClick={onClose}>
        <FontAwesomeIcon icon={faXmark} className="modal-close-icon" />
      </button>
      <div className="popup-title">
        <label className="input-label">TITLE</label>
        <input
          type="text"
          className="note-popup-input"
          placeholder="Go to Gym at 5AM"
          value={title}
          onChange={({ target }) => setTitle(target.value)}
        />
      </div>

      <div className="popup-content">
        <label className="input-label">CONTENT</label>
        <textarea
          className="note-popup-textarea"
          placeholder="Content"
          rows={10}
          value={content}
          onChange={({ target }) => setContent(target.value)}
        />
      </div>

      <div className="popup-tag">
        <label className="input-label">TAGS</label>
        <TagInput tags={tags} setTags={setTags} />
      </div>

      {error && <p className="error-text">{error}</p>}

      <button className="popup-btn" onClick={handleAddNote}>
        {type === "edit" ? "EDIT" : "ADD"}
      </button>
    </div>
  );
}
