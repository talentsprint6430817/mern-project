import React from "react";
import "../../styles/NotesCard.css";

// Icons
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faThumbtack,
  faPenToSquare,
  faTrashCan,
  faMagnifyingGlassPlus
} from "@fortawesome/free-solid-svg-icons";

const NotesCard = ({
  title,
  date,
  content,
  tags,
  isPinned,
  onEdit,
  onDelete,
  onPinNote,
  onFullNote
}) => {
  return (
    <div className="note-container">
      <div className="notes-form">
        <div>
          <h6 className="note-title">{title}</h6>
          <span className="note-date">{date}</span>
        </div>
        <FontAwesomeIcon
          icon={faThumbtack}
          style={{ color: isPinned ? "rgb(129,140,248)" : "rgb(203,213,225)" }}
          className="icon-btn"
          onClick={onPinNote}
        />
      </div>

      <p className="note-content">{content?.slice(0, 60)}</p>

      <div className="note-subContainer">
        <div className="note-tag">{tags.map((tag) => `#${tag} `)}</div>

        <div className="note-btns">
          <FontAwesomeIcon 
          icon={faMagnifyingGlassPlus} 
          className="zoom-icon"
          onClick={onFullNote}
          />
          <FontAwesomeIcon
            icon={faPenToSquare}
            className="note-edit-btn"
            onClick={onEdit}
          />
          <FontAwesomeIcon
            icon={faTrashCan}
            className="note-delete-btn"
            onClick={onDelete}
          />
        </div>
      </div>
    </div>
  );
};

export default NotesCard;
