import React, { useState, useEffect, useContext } from "react";
import { NavLink, useNavigate } from "react-router-dom";
import { AuthContext } from '../AuthProvider';


// Icons
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faFire,
  faStopwatch,
  faNoteSticky,
  faCircleCheck,
  faGear,
  faRightFromBracket,
} from "@fortawesome/free-solid-svg-icons";
import ProfileInfo from "./ProfileInfo";


export default function Sidebar({userInfo}) {
  const navigate = useNavigate();

  const { isLoggedIn, setIsLoggedIn } = useContext(AuthContext); 

  const handleLogout = (e)=>{
    e.preventDefault()
    setIsLoggedIn(false)
    localStorage.clear();
    navigate("/")
  }

  useEffect(() => {
  }, []);


  return (
    <div>
      <div className= "sidebar sidebar-active">
        <div className="head">
          <ProfileInfo userInfo={userInfo}/>
        </div>
        <div className="side-nav">
          <div className="side-nav-menu">
            <p className="title">MENU</p>
            <ul>
              <li>
                <NavLink className="links" activeClassName="active" to="/focus">
                  <FontAwesomeIcon icon={faStopwatch} className="icons" />
                  <span className="text">Focus Session</span>
                </NavLink>
              </li>
              <li>
                <NavLink className="links" activeClassName="active" to="/tasks">
                  <FontAwesomeIcon icon={faCircleCheck} className="icons" />
                  <span className="text">Tasks</span>
                </NavLink>
              </li>
              {/* <li>
                <NavLink
                  className="links"
                  activeClassName="active"
                  to="/habits"
                >
                  <FontAwesomeIcon icon={faFire} className="icons"/>
                  <span className="text">Habits Tracker</span>
                </NavLink>
              </li> */}
              <li>
                <NavLink
                  className="links"
                  activeClassName="active"
                  to="/notes"
                >
                  <FontAwesomeIcon icon={faNoteSticky} className="icons"/>
                  <span className="text">Notes</span>
                </NavLink>
              </li>
            </ul>
          </div>
          <div className="side-nav-menu">
            <p className="title">SETTINGS</p>
            <ul>
              <li>
                <NavLink
                  className="links"
                  activeClassName="active"
                  to="/settings"
                >
                  <FontAwesomeIcon icon={faGear} className="icons" />
                  <span className="text">Settings</span>
                </NavLink>
              </li>
            </ul>
          </div>
        </div>
        <div className="side-nav-menu">
            <p className="title">ACCOUNT</p>
            <ul>
              <li>
                <button className="side-nav-btn links" onClick={handleLogout}>
                  <FontAwesomeIcon icon={faRightFromBracket} className="icons"/>
                  <span className="text">Logout</span>
                </button>
              </li>
            </ul>
          </div>
      </div>
    </div>
  );
}
