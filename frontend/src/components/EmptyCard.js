import React from 'react';
import '../styles/Application.css';

export default function EmptyCard({ imgSrc, message }) {
  return (
    <div className='emptyCard-container'>
        <img src={imgSrc} alt="Nothing to display" className='emptyCard-img' />
        <p className='empty-text'>{message}</p>
    </div>
  )
}
