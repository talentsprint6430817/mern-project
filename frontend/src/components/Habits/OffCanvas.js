import React from 'react';

import { Offcanvas, OffcanvasHeader, OffcanvasBody } from "reactstrap";

export default function OffCanvasHabitsComponent({ isOpen, toggle }) {
  return (
    <div>
      <Offcanvas direction="end" scrollable toggle={toggle} isOpen={isOpen}>
        <OffcanvasHeader toggle={toggle}>
          "Header"
        </OffcanvasHeader>
        {/* <OffcanvasBody>
          <p>{moment(createdOn).format("Do MMM YYYY")}</p>
          <strong>Content</strong>
          <p>{content}</p>
          <strong>Tags</strong>
          <div className="tags-list">
          {tags.map((tag, index) => (
            <span key={index} className="tags">
            # {tag}
          </span>
          ))}
          </div>
        </OffcanvasBody> */}
      </Offcanvas>
    </div>
  )
}
