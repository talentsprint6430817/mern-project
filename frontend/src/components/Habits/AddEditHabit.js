import React, { useState } from 'react';

// * Importing components
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faXmark } from "@fortawesome/free-solid-svg-icons";

export default function AddEditHabit({ onClose, type }) {
  const [title, setTitle] = useState("");
  const [frequency, setFrequency] = useState({});
  const [reminder, setReminder] = useState({});
  const [goalDays, setGoalDays] = useState({});
  const [error, setError] = useState(null);

  const addHabit = () => {}
  const editHabit = () => {}

  // handle 
  const handleAddNote = () => {
    if (!title) {
      setError("Please enter the title");
      return;
    }
    setError("");
    if (type === "edit") {
      editHabit();
    } else {
      addHabit();
    }
  };
 
  return (
    <div className="habits-modal-container">
      <button className="habits-modal-close-btn" onClick={onClose}>
        <FontAwesomeIcon icon={faXmark} className="habits-modal-close-icon" />
      </button>
      <div className="popup-title">
        <label className="habits-input-label">TITLE</label>
        <input
          type="text"
          className="habits-popup-input"
          placeholder="Go to Gym at 5AM"
          // value={title}
          // onChange={({ target }) => setTitle(target.value)}
        />
      </div>

      

      <button className="habits-popup-btn" onClick={handleAddNote}>
        {type === "edit" ? "EDIT" : "ADD"}
      </button>
    </div>

  )
}
