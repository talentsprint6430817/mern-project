import React, { useState, useEffect } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faPenToSquare,
  faTrashCan,
  faMagnifyingGlassPlus,
} from "@fortawesome/free-solid-svg-icons";

import '../../styles/Habits.css';

export default function HabitCard({
  title,
  frequency,
  goalDays,
  streakDays,
  onEdit,
  onDelete,
  onFullNote
}) {
  const [streakState, setStreakState] = useState([]);

  useEffect(() => {
    // Initialize streak state based on goalDays
    if (goalDays.type === 'custom') {
      setStreakState(Array(goalDays.days).fill(false));
    } else {
      setStreakState(Array(7).fill(false));
    }
  }, [goalDays]);

  const handleStreak = (index) => {
    const newStreakState = [...streakState];
    newStreakState[index] = !newStreakState[index];
    setStreakState(newStreakState);

    if (newStreakState[index]) {
      streakDays += 1;
    } else {
      streakDays -= 1;
    }

    console.log(streakDays);
  };

  const renderButtons = () => {
    let buttons = [];

    if (frequency.type === 'daily') {
      buttons = Array.from({ length: 7 }, (_, index) => (
        <button
          key={index}
          className="streak"
          style={{ backgroundColor: streakState[index] ? '#65a30d' : '#f3f4f6' }}
          onClick={() => handleStreak(index)}
        />
      ));
    } else if (frequency.type === 'specificDays' && frequency.specificDays) {
      buttons = frequency.specificDays.map((day, index) => (
        <button
          key={index}
          className="streak"
          style={{ backgroundColor: streakState[index] ? '#65a30d' : '#f3f4f6' }}
          onClick={() => handleStreak(index)}
        >
          {day}
        </button>
      ));
    } else if (frequency.type === 'interval' && frequency.intervalDays) {
      // Display buttons up to the maximum of goalDays.days or 7
      const numButtons = goalDays.type === 'custom' ? Math.min(goalDays.days, 7) : 7;
      buttons = Array.from({ length: numButtons }, (_, index) => (
        <button
          key={index}
          className="streak"
          style={{ backgroundColor: streakState[index] ? '#65a30d' : '#f3f4f6' }}
          onClick={() => handleStreak(index)}
        />
      ));
    }

    return buttons;
  };

  return (
    <div className="habits-container">
      <h6 className="habits-title">{title}</h6>
      <div className="habits-subContainer">
        <div className="habits-streak">
          {renderButtons()}
        </div>
        <div className="habits-btns">
          <FontAwesomeIcon
            icon={faMagnifyingGlassPlus}
            className="zoom-icon"
            onClick={onFullNote}
          />
          <FontAwesomeIcon
            icon={faPenToSquare}
            className="habits-edit-btn"
            onClick={onEdit}
          />
          <FontAwesomeIcon
            icon={faTrashCan}
            className="habits-delete-btn"
            onClick={onDelete}
          />
        </div>
      </div>
    </div>
  );
}
