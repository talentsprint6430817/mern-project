import React, { useState } from "react";

import { NavLink } from "react-router-dom";

import {ReactTyped} from 'react-typed'; // Correct import
import bgVideo from '../assets/bg1.mp4';
import '../styles/Home.css';

// styles
import "../styles/Navbar.css";

const NavbarComponent = () => {
  return (
    <div>
      <div className="herosection">
        <header>
          <nav
            className="navbar navbar-expand-lg navbar-light bg-light fixed-top"
            id="top-nav-styles"
          >
            <NavLink className="navbar-brand me-auto" to="/">
              TaskZen
            </NavLink>
            <button
              className="navbar-toggler pe-0"
              type="button"
              id="top-nav-toggler"
              data-toggle="collapse"
              data-target="#navbarSupportedContent"
              aria-controls="navbarSupportedContent"
              aria-expanded="false"
              aria-label="Toggle navigation"
            >
              <span className="navbar-toggler-icon"></span>
            </button>
            <div
              className="collapse navbar-collapse"
              id="navbarSupportedContent"
            >
              <ul className="navbar-nav mr-auto top-nav-menu">
                <li className="nav-item">
                  <NavLink
                    className="nav-link mx-lg-2"
                    id="top-nav-menu-items"
                    to="/features"
                  >
                    Features
                  </NavLink>
                </li>
                <li className="nav-item">
                  <NavLink
                    className="nav-link mx-lg-2"
                    id="top-nav-menu-items"
                    to="/apps"
                  >
                    Apps
                  </NavLink>
                </li>
                <li className="nav-item">
                  <NavLink
                    className="nav-link mx-lg-2"
                    id="top-nav-menu-items"
                    to="/contact"
                  >
                    Help Center
                  </NavLink>
                </li>
              </ul>
              <NavLink className="btn" id="signup-btn" to="/login">
                Sign In
              </NavLink>
            </div>
          </nav>
        </header>

        <main className="home-main">
          <video autoPlay muted loop id="myVideo">
            <source src={bgVideo} type="video/mp4" />
          </video>
          <div className="overlay home-container home-grid">
            <div className="home-cols0 home-flex">
              <span className="topline dark-text">
                {" "}
                A simple Task management app to manage it all
              </span>
              <h1 className="overlay-title dark-text">
                Do{" "}
                <span className="multiText">
                  <ReactTyped
                    strings={[
                      "Todo Tasks",
                      "Habit Tracking",
                      "Focus Sessions",
                      "Brainstorming of Ideas",
                    ]}
                    typeSpeed={100}
                    backSpeed={80}
                    backDelay={1500}
                    loop
                  />
                </span>
              </h1>
              <p className="overlay-para dark-text">
                Join millions of people to capture ideas, organize life, and do
                something creative everyday.
              </p>
              <div className="overlay-btns">
                {/* <NavLink className="dark-btn btn" to="/login">
                  Get Started
                </NavLink> */}
              </div>
            </div>
          </div>
        </main>
      </div>
    </div>
  );
};

export default NavbarComponent;
