import React from 'react';
import '../styles/Application.css';
import { getInitials } from '../utils/helper';

export default function ProfileInfo({ userInfo }) {
  
  return (
    <div className='profileinfo-container'>
        <div className='profilepic'>{getInitials(userInfo?.fullName)}</div>
    </div>
  )
}
