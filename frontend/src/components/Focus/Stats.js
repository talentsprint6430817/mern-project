import React , { useState } from "react";
import axios from "axios";

// * Importing icons
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faBullseye, faTrash } from "@fortawesome/free-solid-svg-icons";
import ToastMessage from "../ToastMessage";
import axiosInstance from "../../utils/axiosInstance";

// * Importing urls
const url = "http://localhost:4000/focus";

// * Function to format duration in hh:mm:ss format
function formatTimeInMinutesToHHMMSS(minutes) {
  const totalSeconds = minutes * 60;
  const hours = Math.floor(totalSeconds / 3600);
  const minutesRemaining = totalSeconds % 3600;
  const seconds = minutesRemaining % 60;
  if (hours > 0) {
    return `${padTo2(hours)}h ${padTo2(minutesRemaining / 60)} ${padTo2(seconds)}s`;
  } else {
    return `${padTo2(minutesRemaining / 60)}m ${padTo2(seconds)}s`;
  }
}

function padTo2(value) {
  return (value < 10 ? '0' : '') + value;
}

export default function Stats({ sessions, getSessions }) {

   // Group sessions by date
   const sessionsByDate = sessions.reduce((acc, session) => {
    acc[session.date] = acc[session.date] || [];
    acc[session.date].push(session);
    return acc;
  }, {});

  const totalDuration = sessions.reduce((acc, session) => acc + session.duration, 0);

  // Toast Messages
  const [showToast, setShowToast] = useState({
    isShown: false,
    message: "",
    type: "add",
  });
  const showToastMessage = (message, type) => {
    setShowToast({
      isShown: true,
      message,
      type,
    });
  };
  const handleCloseToast = () => {
    setShowToast({
      isShown: false,
      message: "",
    });
  };

  const deleteSession = async (id) => {
    try {
      const response = await axiosInstance.delete('/focus/'+ id);
      if (response.data) {
        showToastMessage("Deleted Successfully", "delete");
        getSessions();
      }
    } catch (error) {
      console.log(error);
    }
  }

  return (
    <div className="stats-container">
      <h3>Overview</h3>
      <div className="overview">
        <div>
          <label className="stat-label">Total Focus</label>
          <p>{sessions.length}</p>
        </div>
        <div>
          <label className="stat-label">Total Focus Duration</label>
          <p>{formatTimeInMinutesToHHMMSS(totalDuration)}</p>
        </div>
      </div>

      <div className="focus-record">
        <h3>Focus Record</h3>
        <div className="focus-record-content">
        {Object.entries(sessionsByDate).map(([date, sessions]) => (
            <>
            <label className="stat-label">{date}</label>
            {sessions.map((record, index)=>(
              <div key={index} className="records-flex">
              <div className="label-record">
                <FontAwesomeIcon icon={faBullseye} className="record-icon" />
                <span className="record-label">{record.label}</span>
                <span className="record-time">{record.startTime}</span>
                <span className="record-time">- {record.endTime}</span>
              </div>
              <div className="update-section">
                {formatTimeInMinutesToHHMMSS(record.duration)}
                <div>
                <FontAwesomeIcon icon={faTrash} className="del-icon" onClick={()=>deleteSession(record._id)}/>
                </div>
              </div>
            </div>

            ))}
            </>
            
          ))}
        </div>
      </div>
      <ToastMessage
      isShown={showToast.isShown}
      message={showToast.message}
      type={showToast.type}
      onClose={handleCloseToast}
      />
    </div>
  );
}
