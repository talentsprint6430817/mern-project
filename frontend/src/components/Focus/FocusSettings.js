import React, { useContext } from "react";
import ReactSlider from 'react-slider';

// * Importing components
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faXmark } from "@fortawesome/free-solid-svg-icons";
import {SettingsContext} from "../Focus/FocusProvider";


export default function FocusSettings({ onClose }) {
  // use context
  const settingsInfo = useContext(SettingsContext)

  return (
    <div>
      <div className="modal-container">
        <button className="modal-close-btn" onClick={onClose}>
          <FontAwesomeIcon icon={faXmark} className="modal-close-icon" />
        </button>

        {/* For setting time */}
        <div className="slider-container">
            <label>Focus : {settingsInfo.focusMinutes}:00</label>
            <ReactSlider 
            className={'slider'} 
            thumbClassName={"thumb"} 
            trackClassName={"track"} 
            value={settingsInfo.focusMinutes} 
            onChange={newValue => settingsInfo.setFocusMinutes(newValue)}
            min={1} 
            max={180}/>
        </div>
        
        <div className="popup-title">
          <label className="input-label">LABEL</label>
          <input
            type="text"
            className="focus-popup-input"
            placeholder="Study session"
            value={settingsInfo.label} 
            onChange={e => settingsInfo.setLabel(e.target.value)}
          />
        </div>
      </div>
    </div>
  );
}
