import { useState, useEffect, useRef } from 'react';

export const useFocusTimer = (focusMinutes, isPausedRef, switchMode) => {
  const [secondsLeft, setSecondsLeft] = useState(focusMinutes * 60);
  const secondsLeftRef = useRef(secondsLeft);

  useEffect(() => {
    secondsLeftRef.current = focusMinutes * 60;
    setSecondsLeft(secondsLeftRef.current);

    const interval = setInterval(() => {
      if (isPausedRef.current) {
        return;
      }
      if (secondsLeftRef.current === 0) {
        switchMode();
        return;
      }

      secondsLeftRef.current--;
      setSecondsLeft(secondsLeftRef.current);
    }, 1000);

    return () => clearInterval(interval);
  }, [focusMinutes, isPausedRef, switchMode]);

  return secondsLeft;
};
