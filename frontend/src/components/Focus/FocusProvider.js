import React, { createContext, useState } from "react";

const SettingsContext = createContext({});

const SettingsProvider = ({ children }) =>{
    // focus minutes and break minutes
    const [focusMinutes, setFocusMinutes] = useState(25);
    const [label, setLabel] = useState("")

    return (
        <SettingsContext.Provider value={{ focusMinutes, setFocusMinutes, label, setLabel }}>
            {children}
        </SettingsContext.Provider>
    )
}
export { SettingsContext, SettingsProvider};