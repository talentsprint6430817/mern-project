import React from "react";

//* Icons and Styles
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faFlag,
  faPenToSquare,
  faTrashCan,
} from "@fortawesome/free-solid-svg-icons";

export default function TaskCard({
  title,
  due,
  tags,
  isCompleted,
  priority,
  onEdit,
  onDelete,
  onComplete
}) {
  return (
    <div className="taskCard-container">
      <div className="taskCard-form">
        <div>
          <h6 className="taskCard-title">{title}</h6>
          <span className="taskCard-date"><p>Due on:</p>{due}</span>
        </div>
      </div>

       <div className="taskCard-tag">{tags.map((tag) => `#${tag} `)}</div>

      <div className="taskCard-subContainer">

      <button 
      style={{ backgroundColor: isCompleted ? "rgb(22,163,74)" : "rgb(239,68,68)" }} 
      className="completed-btn"
      onClick={onComplete}
      >
        {isCompleted ? 'Completed' : 'Incomplete'}
      </button>
        
        <div className="taskCard-btns">
          <FontAwesomeIcon
            icon={faFlag}
            className="taskCard-priority-btn"
            style={{ color: priority === 'low' ? "rgb(22,163,74)" : (priority === 'medium' ? "#fbbf24" : "rgb(239,68,68)") }}
          />
          <FontAwesomeIcon
            icon={faPenToSquare}
            className="taskCard-edit-btn"
            onClick={onEdit}
          />
          <FontAwesomeIcon
            icon={faTrashCan}
            className="taskCard-delete-btn"
            onClick={onDelete}
          />
        </div>
      </div>
    </div>
  );
}
