import React, { useState, useEffect } from "react";
import axios from "axios";
import TagInput from "../TagInput";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faXmark } from "@fortawesome/free-solid-svg-icons";
import { Input } from "reactstrap";
import axiosInstance from "../../utils/axiosInstance";

// Importing URLs
const url_getTasks = "http://localhost:4000/tasks";

export default function AddEditTask({
  type,
  taskData,
  getAllTasks,
  onClose,
  showToastMessage,
}) {
  const [title, setTitle] = useState("");
  const [tags, setTags] = useState([]);
  const [priority, setPriority] = useState("low");
  const [error, setError] = useState(null);
  
  const [due, setDue] = useState(() => {
    // Initialize with current date formatted as YYYY-MM-DD
    const today = new Date();
    return today.toISOString().split("T")[0];
  });

 
  const addNewTask = async () => {
    try {
      const response = await axiosInstance.post('/tasks', {
        title,
        tags,
        priority,
        due,
      });
      if (response.data) {
        showToastMessage("Added successfully", "add");
        getAllTasks();
        onClose();
      }
    } catch (error) {
      if (error.response && error.response.data && error.response.data.message) {
        setError(error.response.data.message);
      }
    }
  };

  const editTask = async () => {
    try {
      const response = await axiosInstance.put('/tasks/'+ taskData._id, {
        title,
        tags,
        priority,
        due,
      });
      if (response.data) {
        showToastMessage("Updated successfully", "edit");
        getAllTasks();
        onClose();
      }
    } catch (error) {
      if (error.response && error.response.data && error.response.data.message) {
        setError(error.response.data.message);
      }
    }
  };

  const handleAddTask = () => {
    if (!title) {
      setError("Please enter the title");
      return;
    }
    setError("");
    if (type === "edit") {
      editTask();
    } else {
      addNewTask();
    }
  };

  // Populate fields if editing an existing task
  useEffect(() => {
    if (type === "edit" && taskData) {
      setTitle(taskData.title);
      setTags(taskData.tags);
      setPriority(taskData.priority);
      setDue(taskData.due ? taskData.due.split("T")[0] : ""); // Format due date if it exists
    }
  }, [type, taskData]);

  return (
    <div className="modal-task-container">
      <button className="modal-task-close-btn" onClick={onClose}>
        <FontAwesomeIcon icon={faXmark} className="modal-task-close-icon" />
      </button>
      <div className="popup-task-title">
        <label className="input-task-label">TITLE</label>
        <input
          type="text"
          className="task-popup-input"
          placeholder="Go to Gym at 5AM"
          value={title}
          onChange={({ target }) => setTitle(target.value)}
        />
      </div>
      <div className="popup-task-date">
        <label className="input-task-label">DUE ON:</label>
        <input
          type="date"
          className="task-date-input"
          value={due}
          onChange={({ target }) => setDue(target.value)}
        />
      </div>

      <div className="popup-task-tag">
        <label className="input-task-label">TAGS</label>
        <TagInput tags={tags} setTags={setTags} />
      </div>

      <div className="popup-task-priority">
        <form>
          <label className="input-task-label">PRIORITY</label>
          <Input
            id="select"
            name="select"
            type="select"
            value={priority}
            onChange={(e) => setPriority(e.target.value)}
          >
            <option value="low">Low</option>
            <option value="medium">Medium</option>
            <option value="high">High</option>
          </Input>
        </form>
      </div>

      {error && <p className="error-task-text">{error}</p>}

      <button className="popup-task-btn" onClick={handleAddTask}>
        {type === "edit" ? "EDIT" : "ADD"}
      </button>
    </div>
  );
}
