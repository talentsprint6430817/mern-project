import React from "react";
import { Form, FormGroup, Input } from "reactstrap";

export default function Filtered({
  getAllTasks,
  getFilterAsc,
  getFilterDesc,
  getStatusCompleted,
  getStatusInComplete,
  getPriority,
}) {
  const handleFilterChange = (event) => {
    if (event.target.value === "all") {
      getAllTasks();
    } else if (event.target.value === "dueAsc") {
        getFilterAsc();
    } else if (event.target.value === "dueDesc") {
        getFilterDesc();
    } else if (event.target.value === "completed") {
        getStatusCompleted();
    } else if (event.target.value === "incomplete") {
        getStatusInComplete();
    } else if (event.target.value === "priority") {
        getPriority();
    }

  };

  return (
    <div className="filter-container">
      <Form>
        <FormGroup>
          <Input
            id="filterSelect"
            name="filter"
            type="select"
            onChange={handleFilterChange}
          >
            <option value="all">All Tasks</option>
            <option value="dueAsc">Sort by Due Dates (Ascending)</option>
            <option value="dueDesc">Sort by Due Dates (Descending)</option>
            <option value="completed">Completed Tasks</option>
            <option value="incomplete">Incomplete Tasks</option>
            <option value="priority">Sort by Priority</option>
          </Input>
        </FormGroup>
      </Form>
    </div>
  );
}
