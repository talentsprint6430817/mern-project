import React, { useEffect } from "react";
import '../styles/Application.css';

// * Importing icons
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCheck, faXmark } from "@fortawesome/free-solid-svg-icons";

export default function ToastMessage({ isShown, message, type, onClose }) {
    const toastClass = type === "delete" ? "delete" : "default";

  useEffect(() => {
    const timeoutId = setTimeout(() => {
      onClose();
    }, 3000);

    return () => {
      clearTimeout(timeoutId);
    };
  }, [onClose]);

  return (
    <div
      className="toast-container"
      style={{
        opacity: isShown ? 1 : 0,
        transition: "0.5s ease-in-out",
      }}
    >
      <div className={`toast-container-styles ${toastClass}`}>
        <div className="toast-container-flex">
          <div
            className="toast-icon-style"
            style={{
              backgroundColor: type === "delete" ? "#f87171" : "#bbf7d0",
            }}
          >
            {type === "delete" ? (
              <FontAwesomeIcon icon={faXmark} className="toast-icon-x" />
            ) : (
              <FontAwesomeIcon icon={faCheck} className="toast-icon-t" />
            )}
          </div>
          <p className="toast-msg">{message}</p>
        </div>
      </div>
    </div>
  );
}
