import React, { useEffect, useState } from "react";
import axiosInstance from "../utils/axiosInstance";
import moment from "moment";
import Modal from "react-modal";

// * Importing components
import TaskCard from "../components/Tasks/TaskCard";
import ToastMessage from "../components/ToastMessage";
import AddEditTask from "../components/Tasks/AddEditTask";
import EmptyCard from "../components/EmptyCard";
import Filtered from "../components/Tasks/Filtered";

// * Import icons and images
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPlus } from "@fortawesome/free-solid-svg-icons";
import AddSomething from "../assets/AddSomething.jpg";
import notFound from "../assets/notFound.jpg";

export default function AxiosTasks() {
  const [tasks, setTasks] = useState([]);
  const [filter, setFilter] = useState(false);
  const [showToast, setShowToast] = useState({
    isShown: false,
    message: "",
    type: "add",
  });
  const [openAddEditModal, setOpenAddEditModal] = useState({
    isShown: false,
    type: "add",
    data: null,
  });

  const showToastMessage = (message, type) => {
    setShowToast({
      isShown: true,
      message,
      type,
    });
  };

  const handleCloseToast = () => {
    setShowToast({
      isShown: false,
      message: "",
    });
  };

  // onEdit function
  const handleEdit = (taskDetails) => {
    setOpenAddEditModal({ isShown: true, type: "edit", data: taskDetails });
  };

  // getting all tasks
  const getAllTasks = async () => {
    try {
      const response = await axiosInstance.get('/tasks');
      if (response.data) {
        setTasks(response.data);
      }
    } catch (err) {
      console.log(err);
    }
  };

  // Delete a task
  const deleteTask = async (data) => {
    try {
      const response = await axiosInstance.delete('/tasks/' + data._id);
      if (response.data) {
        showToastMessage("Deleted Successfully", "delete");
        getAllTasks();
      }
    } catch (error) {
      if (
        error.response &&
        error.response.data &&
        error.response.data.message
      ) {
        console.log(error);
      }
    }
  };

  // complete a task
  const updateStatus = async (taskData) => {
    try {
      const response = await axiosInstance.put('/tasks/' + taskData._id, {
        isCompleted: !taskData.isCompleted,
      });
      if (response.data) {
        showToastMessage("Updated Successfully", "status");
        getAllTasks();
      }
    } catch (error) {
      console.log(error);
    }
  };

  // Filters
  const getFilterAsc = async () => {
    try {
      const response = await axiosInstance.get("/tasks/filterAsc");
      if (response.data && response.data.tasks) {
        setTasks(response.data.tasks);
        setFilter(true);
      }
    } catch (error) {
      console.log(error);
    }
  };

  const getFilterDesc = async () => {
    try {
      const response = await axiosInstance.get("/tasks/filterDesc");
      if (response.data && response.data.tasks) {
        setTasks(response.data.tasks);
        setFilter(true);
      }
    } catch (error) {
      console.log(error);
    }
  };

  const getStatusCompleted = async () => {
    try {
      const response = await axiosInstance.get("/tasks/status", { params: { status: "true" }});
      if (response.data && response.data.tasks) {
        setTasks(response.data.tasks);
        setFilter(true);
      }
    } catch (error) {
      console.log(error);
    }
  };

  const getStatusInComplete = async () => {
    try {
      const response = await axiosInstance.get("/tasks/status", { params: { status: "false" }});
      if (response.data && response.data.tasks) {
        setTasks(response.data.tasks);
        setFilter(true);
      }
    } catch (error) {
      console.log(error);
    }
  };

  const getPriority = async () => {
    try {
      const response = await axiosInstance.get("/tasks/priority");
      if (response.data && response.data.tasks) {
        setTasks(response.data.tasks);
        setFilter(true);
      }
    } catch (error) {
      console.log(error);
    }
  };

  // use effect
  useEffect(() => {
    getAllTasks();
  }, []);

  return (
    <>
      {/* Filter icon */}
      <div>
        <Filtered 
        getAllTasks={getAllTasks}
        getFilterAsc={getFilterAsc}
        getFilterDesc={getFilterDesc}
        getStatusCompleted={getStatusCompleted}
        getStatusInComplete={getStatusInComplete}
        getPriority={getPriority}
         />
      </div>
      {/* tasks grid */}
      {tasks.length > 0 ? (
        <div className="tasks-grid-container">
          {tasks.map((task) => (
            <TaskCard
              key={task._id}
              title={task.title}
              due={moment(task.due).format("Do MMM YYYY")}
              tags={task.tags}
              isCompleted={task.isCompleted}
              priority={task.priority}
              onEdit={() => handleEdit(task)}
              onDelete={() => deleteTask(task)}
              onComplete={() => updateStatus(task)}
            />
          ))}
        </div>
      ) : (
        <EmptyCard
          imgSrc={filter ? notFound : AddSomething}
          message={filter ?`No Such Tasks found`:`Start creating your first Task! Click the 'Add' button to add a To Do Task. Let's get started!`}
        />
      )}

      {/* This is for opening a Modal for entering data */}
      <Modal
        isOpen={openAddEditModal.isShown}
        onRequestClose={() => {
          setOpenAddEditModal({ isShown: false, type: "add", data: null });
        }}
        style={{ overlay: { backgroundColor: "rgba(0,0,0,0.2)" } }}
        contentLabel=""
        className="tasks-modal"
      >
        <AddEditTask
          type={openAddEditModal.type}
          taskData={openAddEditModal.data}
          getAllTasks={getAllTasks}
          onClose={() => {
            setOpenAddEditModal({ isShown: false, type: "add", data: null });
          }}
          showToastMessage={showToastMessage}
        />
      </Modal>

      {/* This is for adding a Task / opening a modal */}
      <button
        className="add-tasks-btn"
        onClick={() => {
          setOpenAddEditModal({ isShown: true, type: "add", data: null });
        }}
      >
        <FontAwesomeIcon icon={faPlus} className="addTasks-icon" />
      </button>

      {/* Toast Message */}
      <ToastMessage
        isShown={showToast.isShown}
        message={showToast.message}
        type={showToast.type}
        onClose={handleCloseToast}
      />
    </>
  );
}
