import React, { useState, useContext } from "react";
import { Form, FormGroup, Label, Input, Button } from "reactstrap";
import { useNavigate } from "react-router-dom";
import { validateEmail } from "../utils/helper";
import axiosInstance from "../utils/axiosInstance";
import { AuthContext } from "../AuthProvider";

import "../styles/Auth.css";

export default function AxiosAuth() {
  const { isLoggedIn, setIsLoggedIn } = useContext(AuthContext);
  const navigate = useNavigate();

  const [isLogin, setIsLogin] = useState(true);
  const [username, setUsername] = useState("");
  const [registerEmail, setRegisterEmail] = useState("");
  const [registerPassword, setRegisterPassword] = useState("");

  const [loginEmail, setLoginEmail] = useState("");
  const [loginPassword, setLoginPassword] = useState("");

  const [error, setError] = useState(null);

  const handleSlideLogin = (e) => {
    e.preventDefault();
    setIsLogin(true);
  };

  const handleSlideSignup = (e) => {
    e.preventDefault();
    setIsLogin(false);
  };

  // Sign up
  const handleSignup = async (e) => {
    e.preventDefault();

    if (!username) {
      setError("Please enter your name");
      return;
    }
    if (!validateEmail(registerEmail)) {
      setError("Please enter a valid email");
      return;
    }
    if (!registerPassword) {
      setError("Please enter the password");
      return;
    }
    setError("");

    const requestData = {
      fullName: username,
      email: registerEmail,
      password: registerPassword,
    };

    try {
      const response = await axiosInstance.post("/auth/signup", {
        fullName: username,
        email: registerEmail,
        password: registerPassword,
      });

      // successfull registeration
      if (response.data && response.data.token) {
        localStorage.setItem("token", response.data.token);
        setIsLoggedIn(true);
        setError(""); // Clear any previous errors
        navigate("/tasks");
      }
    } catch (error) {
      console.log(error);
      setError("An error occurred during login.");
    }
  };

  const handleLogin = async (e) => {
    e.preventDefault();

    if (!validateEmail(loginEmail)) {
      setError("Please enter a valid email");
      return;
    }
    if (!loginPassword) {
      setError("Please enter the password");
      return;
    }
    setError("");

    try {
      const response = await axiosInstance.post("/auth/login", {
        email: loginEmail,
        password: loginPassword,
      });
      console.log(response.data); // Debug: log the response data
      if (response.data && response.data.token) {
        localStorage.setItem("token", response.data.token);
        setIsLoggedIn(true);
        setError(""); // Clear any previous errors
        navigate("/tasks");
      } else {
        setError("Login failed. Please check your credentials.");
      }
    } catch (error) {
      console.log(error);
      setError("An error occurred during login.");
    }
  };

  return (
    <div className="auth-body">
      <div className="form-container">
        <div className={`signup-container ${isLogin ? "slide-up" : ""}`}>
          <button className="signup-btn" onClick={handleSlideSignup}>
            {isLogin ? "Create an account" : <h2 id="sign-up">Sign Up</h2>}
          </button>
          {!isLogin && (
            <div className="form-holder">
              <Form>
              <FormGroup>
                  <Label for="exampleEmail" hidden>Username<span className="required">*</span>
                  </Label>
                  <Input
                    id="exampleEmail"
                    name="email"
                    placeholder="Username"
                    type="text"
                    value={username}
                    onChange={(e) => setUsername(e.target.value)}
                    required
                  />
                </FormGroup>{" "}
                <FormGroup>
                  <Label for="exampleEmail" hidden>
                    Email<span className="required">*</span>
                  </Label>
                  <Input
                    id="exampleEmail"
                    name="email"
                    placeholder="Email"
                    type="email"
                    value={registerEmail}
                    onChange={(e) => setRegisterEmail(e.target.value)}
                    required
                  />
                </FormGroup>{" "}
                <FormGroup>
                  <Label for="examplePassword" hidden>
                    Password<span className="required">*</span>
                  </Label>
                  <Input
                    id="examplePassword"
                    name="password"
                    placeholder="Password"
                    type="password"
                    value={registerPassword}
                    onChange={(e) => setRegisterPassword(e.target.value)}
                    required
                  />
                </FormGroup>{" "}
                {error && <p className="error-text">{error}</p>}
                <button className="submit-btn" onClick={handleSignup}>
                  Sign Up
                </button>
              </Form>
            </div>
          )}
        </div>
        <div className={`login-container ${isLogin ? "" : "slide-up"}`}>
          <div className="login-wrapper">
            <button className="title-btn" onClick={handleSlideLogin}>
              <h2 className="form-title" id="login">
                {isLogin ? "Log In" : "Already have an account?"}
              </h2>
            </button>
            {isLogin && (
              <Form className="form-holder">
                <FormGroup floating>
                  <Input
                    id="exampleEmail"
                    name="email"
                    placeholder="Email"
                    type="email"
                    value={loginEmail}
                    onChange={(e) => setLoginEmail(e.target.value)}
                    required
                  />
                  <Label for="exampleEmail">
                    Email<span className="required">*</span>
                  </Label>
                </FormGroup>
                <FormGroup floating>
                  <Input
                    id="examplePassword"
                    name="password"
                    placeholder="Password"
                    type="password"
                    value={loginPassword}
                    onChange={(e) => setLoginPassword(e.target.value)}
                    required
                  />
                  <Label for="examplePassword">
                    Password<span className="required">*</span>
                  </Label>
                </FormGroup>
                {error && <p className="error-text">{error}</p>}
                <button className="submit-btn" onClick={handleLogin}>
                  Log in
                </button>
                <a href="#" className="forgot-password">
                  Forgot Password?
                </a>
              </Form>
            )}
          </div>
        </div>
      </div>
    </div>
  );
}
