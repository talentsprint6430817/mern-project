import React, { useContext, useEffect, useState, useRef } from "react";
import axiosInstance from "../utils/axiosInstance";
import {
  buildStyles,
  CircularProgressbarWithChildren,
} from "react-circular-progressbar";
import "react-circular-progressbar/dist/styles.css";
import Modal from "react-modal";

// * Import components
import RadialSeparators from "../components/Focus/RadialSeparators";
import FocusSettings from "../components/Focus/FocusSettings";
import {SettingsContext} from "../components/Focus/FocusProvider";
import Stats from "../components/Focus/Stats";
import ToastMessage from "../components/ToastMessage";

// * Import styles and icons
import "../styles/Focus.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faCirclePlay,
  faCircleStop,
} from "@fortawesome/free-regular-svg-icons";

// * Define colors
const indigo = "rgb(129,140,248)";
const black = "rgb(0,0,0)";


export default function AxiosFocus() {
  const settingsInfo = useContext(SettingsContext);

  // getting all sessions
  const [sessions, setSessions] = useState([]);
  const getSessions = async () => {
    axiosInstance
      .get('/focus')
      .then((res) => {
        setSessions(res.data);
      })
      .catch((err) => {
        console.log(err);
      });
  };

  // Recording a new session
  const addSession = async (newSession) => {
    try {
      const response = await axiosInstance.post('/focus', newSession);
      if (response.data) {
        showToastMessage("Session recorded Successfully", "add");
        getSessions();
      }
    } catch (error) {
      if (error.response && error.response.data && error.response.data.error) {
        console.log(error.response.data.error);
      } else {
        console.log(error.message);
      }
    }
  };

  // handle adding the focus sessions
  const [showSettings, setShowSettings] = useState({
    isShown: false,
    type: "add",
    data: null,
  });

  const handleFocusSettings = () => {
    setShowSettings({
      isShown: true,
      type: "add",
      data: null,
    });
  };

  const onClose = () => {
    setShowSettings({
      isShown: false,
      type: "add",
      data: null,
    });
  };

  const [isPaused, setIsPaused] = useState(true);
  const [secondsLeft, setSecondsLeft] = useState(0);

  const secondsLeftRef = useRef(secondsLeft);
  const isPausedRef = useRef(isPaused);

  function tick() {
    secondsLeftRef.current--;
    setSecondsLeft(secondsLeftRef.current);
  }

  useEffect(() => {
    getSessions();

    const switchMode = () => {
      setIsPaused(true);
      isPausedRef.current = true;

      const currentTime = new Date();
      const endTime = currentTime.toLocaleTimeString();

      // Calculate the start time by subtracting the duration from the end time
      const endTimeDate = new Date(currentTime);
      endTimeDate.setMinutes(
        endTimeDate.getMinutes() - settingsInfo.focusMinutes
      );
      const calculatedStartTime = endTimeDate.toLocaleTimeString();

      const newSession = {
        label: settingsInfo.label,
        duration: settingsInfo.focusMinutes,
        startTime: calculatedStartTime,
        endTime: endTime,
        date: currentTime.toLocaleDateString(), // Add this line to include the date
      };

      // Add the new session to the state
      addSession(newSession);
      settingsInfo.setLabel("");
    };

    secondsLeftRef.current = settingsInfo.focusMinutes * 60;
    setSecondsLeft(secondsLeftRef.current);

    const interval = setInterval(() => {
      if (isPausedRef.current) {
        return;
      }
      if (secondsLeftRef.current === 0) {
        switchMode();
        return;
      }

      tick();
    }, 1000);

    return () => clearInterval(interval);
  }, [settingsInfo]);

  const totalSeconds = settingsInfo.focusMinutes * 60;
  const percentage = Math.round((secondsLeft / totalSeconds) * 100);

  const minutes = Math.floor(secondsLeft / 60);
  let seconds = secondsLeft % 60;
  if (seconds < 10) seconds = "0" + seconds;

  const handleStart = () => {
    setIsPaused(false);
    isPausedRef.current = false;
  };

  const handlePause = () => {
    setIsPaused(true);
    isPausedRef.current = true;
    secondsLeftRef.current = settingsInfo.focusMinutes * 60;
    setSecondsLeft(secondsLeftRef.current);
  };

  // Toast Messages
  const [showToast, setShowToast] = useState({
    isShown: false,
    message: "",
    type: "add",
  });
  const showToastMessage = (message, type) => {
    setShowToast({
      isShown: true,
      message,
      type,
    });
  };
  const handleCloseToast = () => {
    setShowToast({
      isShown: false,
      message: "",
    });
  };

  return (
    <div className="focus-container">
      {/* Left container */}
      <div className="progressbar-container">
        <button className="focus-title" onClick={handleFocusSettings}>
          <h2>Start to Focus!</h2>
        </button>
        <div className="progressbar">
          <CircularProgressbarWithChildren
            value={percentage}
            text={`${minutes}:${seconds}`}
            strokeWidth={10}
            styles={buildStyles({
              textColor: black,
              strokeLinecap: "butt",
              pathColor: indigo,
            })}
          >
            <RadialSeparators
              count={12}
              style={{
                background: "#fff",
                width: "2px",
                height: `${10}%`,
              }}
            />
          </CircularProgressbarWithChildren>
        </div>
        <div className="btns">
          {isPaused ? (
            <button className="play-btn" onClick={handleStart}>
              <FontAwesomeIcon
                id="play"
                icon={faCirclePlay}
                className="play-icon"
              />
            </button>
          ) : (
            <button className="play-btn" onClick={handlePause}>
              <FontAwesomeIcon
                id="pause"
                icon={faCircleStop}
                className="play-icon"
              />
            </button>
          )}
        </div>

        {/* Modal view for settings */}
        <Modal
          isOpen={showSettings.isShown}
          onRequestClose={() => {}}
          style={{
            overlay: {
              backgroundColor: "rgba(0,0,0,0.2)",
            },
          }}
          contentLabel=""
          className="focus-modal"
        >
          <FocusSettings onClose={onClose} />
        </Modal>

        <ToastMessage
          isShown={showToast.isShown}
          message={showToast.message}
          type={showToast.type}
          onClose={handleCloseToast}
        />
      </div>

      {/* Right container */}
      <div className="focus-stats-container">
        <Stats sessions={sessions} getSessions={getSessions} />
      </div>
    </div>
  );
}


