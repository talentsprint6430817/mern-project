import React, { useEffect, useState } from "react";
import axios from "axios";
import moment from "moment";
import Modal from "react-modal";

// * Importing components
import HabitCard from "../components/Habits/HabitCard";
import AddEditHabit from "../components/Habits/AddEditHabit";
import ToastMessage from "../components/ToastMessage";

// * Importing icons and images
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPlus } from "@fortawesome/free-solid-svg-icons";

export default function AxiosHabits({userInfo}) {
  // Toast Messages
  const [showToast, setShowToast] = useState({
    isShown: false,
    message: "",
    type: "add",
  });
  const showToastMessage = (message, type) => {
    setShowToast({
      isShown: true,
      message,
      type,
    });
  };
  const handleCloseToast = () => {
    setShowToast({
      isShown: false,
      message: "",
    });
  };

  // For opening modal
  const [openAddEditModal, setOpenAddEditModal] = useState({
    isShown: false,
    type: "add",
    data: null,
  });

  //To open offcanvas components
  const [isOpen, setIsOpen] = useState(false);
  const [isOffCanvas, setIsOffCanvas] = useState({
    title: "",
    content: "",
    createdOn: "",
    tags: [],
  });

  // toggle function of offcanvas
  const toggle = () => setIsOpen(!isOpen);

  return (
    <>
      {/* Habits grid */}
      <div className="habits-grid-container">
        <HabitCard
          title={"Sample Title"}
          frequency={{intervalDays:2, type:"interval"}}
          goalDays={7}
          streakDays={0}
          onEdit={() => {}}
          onDelete={() => {}}
          onFullNote={() => toggle()}
        />
      </div>

      {/* This is for opening a Modal for entering data */}
      <Modal
        isOpen={openAddEditModal.isShown}
        onRequestClose={() => {
          setOpenAddEditModal({ isShown: false, type: "add", data: null });
        }}
        style={{ overlay: { backgroundColor: "rgba(0,0,0,0.2)" } }}
        contentLabel=""
        className="habits-modal"
      >
        <AddEditHabit 
        type={openAddEditModal.type}
        onClose={() => {
          setOpenAddEditModal({ isShown: false, type: "add", data: null });
        }}
        />
      </Modal>

      {/* This is for adding a Habit/ opening a modal*/}
      <button
        className="add-habits-btn"
        onClick={() => {
          setOpenAddEditModal({ isShown: true, type: "add", data: null });
        }}
      >
        <FontAwesomeIcon icon={faPlus} className="addHabits-icon" />
      </button>

      {/* Toast Message */}
      <ToastMessage
        isShown={showToast.isShown}
        message={showToast.message}
        type={showToast.type}
        onClose={handleCloseToast}
      />

      {/* Off canvas */}
      {/* <OffCanvasComponent
        title={isOffCanvas.title}
        content={isOffCanvas.content}
        tags={isOffCanvas.tags}
        createdOn={isOffCanvas.createdOn}
        isOpen={isOpen} // Pass isOpen to OffCanvasComponent
        toggle={toggle} // Pass toggle to OffCanvasComponent
      /> */}
    </>
  );
}
