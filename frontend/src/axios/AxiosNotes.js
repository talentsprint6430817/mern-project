import React, { useEffect, useState } from "react";
import moment from "moment";
import Modal from "react-modal";
import axiosInstance from "../utils/axiosInstance";

// * Importing components
import NotesCard from "../components/Notes/NotesCard";
import AddEditNote from "../components/Notes/AddEditNote";
import EmptyCard from "../components/EmptyCard";
import SearchBar from "../components/Notes/SearchBar";
import ToastMessage from "../components/ToastMessage";
import OffCanvasComponent from "../components/Notes/OffCanvas";
import FilterBy from "../components/Notes/FilterBy";

// * Importing images and icons
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPlus } from "@fortawesome/free-solid-svg-icons";
import AddSomething from "../assets/AddSomething.jpg";
import notFound from "../assets/notFound.jpg";




const AxiosNotes = () => {
  const [pinnedFilter, setPinnedFilter] = useState(false);
  const [notes, setNotes] = useState([]);
  const [searchQuery, setSearchQuery] = useState("");
  const [isSearch, setIsSearch] = useState(false);
  const [showToast, setShowToast] = useState({
    isShown: false,
    message: "",
    type: "add",
  });
  const [openAddEditModal, setOpenAddEditModal] = useState({
    isShown: false,
    type: "add",
    data: null,
  });
  const [isOpen, setIsOpen] = useState(false);
  const [isOffCanvas, setIsOffCanvas] = useState({
    title: "",
    content: "",
    createdOn: "",
    tags: [],
  });

  // Toast Messages
  const showToastMessage = (message, type) => {
    setShowToast({
      isShown: true,
      message,
      type,
    });
  };
  const handleCloseToast = () => {
    setShowToast({
      isShown: false,
      message: "",
    });
  };

  
// Notes
  const handleEdit = (noteDetails) => {
    setOpenAddEditModal({ isShown: true, type: "edit", data: noteDetails });
  };

  const getAllNotes = async () => {
    try {
      const response = await axiosInstance.get("/notes/");
      if (response.data) {
        setNotes(response.data);
        setPinnedFilter(false)
      }
    } catch (error) {
      console.log(error);
    }
  };

  const getPinned = async () => {
    try {
      const response = await axiosInstance.get("/notes/pinned");
      if (response.data && response.data.filtered) {
        setNotes(response.data.filtered);
        setPinnedFilter(true)
      }
    } catch (error) {
      console.log(error);
    }
  };

  const deleteNote = async (data) => {
    try {
      const response = await axiosInstance.delete('/notes/'+ data._id);
      if (response.data) {
        showToastMessage("Deleted Successfully", "delete");
        getAllNotes();
      }
    } catch (error) {
      if (
        error.response &&
        error.response.data &&
        error.response.data.message
      ) {
        console.log(error);
      }
    }
  };

  const onSearchNote = async (query) => {
    try {
      const response = await axiosInstance.get("/notes/searchnote", {
        params: { query },
      });
      if (response.data && response.data.notes) {
        setIsSearch(true);
        setNotes(response.data.notes);
      }
    } catch (error) {
      console.log(error);
    }
  };

  const handleSearch = () => {
    if (searchQuery) {
      onSearchNote(searchQuery);
    }
  };

  const onClearSearch = () => {
    setSearchQuery("");
    setIsSearch(false);
    getAllNotes();
  };

  const UpdateisPinned = async (noteData) => {
    try {
      const response = await axiosInstance.put('/notes/pinned/'+ noteData._id, {
        isPinned: !noteData.isPinned,
      });
      if (response.data) {
        getAllNotes();
      }
    } catch (error) {
      console.log(error);
    }
  };

  // OffCanvas
  const toggle = () => setIsOpen(!isOpen);

  const handleOffCanvas = (noteData) => {
    setIsOffCanvas({
      title: noteData.title,
      content: noteData.content,
      createdOn: noteData.createdOn,
      tags: noteData.tags,
    });
    toggle();
  };

  useEffect(() => {
    getAllNotes();
  }, []);

  return (
    <>
      <SearchBar
        value={searchQuery}
        onChange={({ target }) => {
          setSearchQuery(target.value);
        }}
        handleSearch={handleSearch}
        onClearSearch={onClearSearch}
      />

      <FilterBy
      getAllNotes={getAllNotes}
      getPinned={getPinned}
       />

      {notes.length > 0 ? (
        <div className="notes-grid-container">
          {notes.map((note) => (
            <NotesCard
              key={note._id}
              title={note.title}
              date={moment(note.createdOn).format("Do MMM YYYY")}
              content={note.content}
              tags={note.tags}
              isPinned={note.isPinned}
              onEdit={() => handleEdit(note)}
              onDelete={() => deleteNote(note)}
              onPinNote={() => UpdateisPinned(note)}
              onFullNote={() => handleOffCanvas(note)}
            />
          ))}
        </div>
      ) : (
        <EmptyCard
          imgSrc={isSearch || pinnedFilter ? notFound : AddSomething}
          message={
            isSearch || pinnedFilter 
              ? `Oops! No notes found`
              : `Start creating your first note! Click the 'Add' button to jot down your thoughts, ideas and reminders. Let's get started!`
          }
        />
      )}

      <Modal
        isOpen={openAddEditModal.isShown}
        onRequestClose={() => {
          setOpenAddEditModal({ isShown: false, type: "add", data: null });
        }}
        style={{ overlay: { backgroundColor: "rgba(0,0,0,0.2)" } }}
        contentLabel=""
        className="notes-modal"
      >
        <AddEditNote
          type={openAddEditModal.type}
          noteData={openAddEditModal.data}
          getAllNotes={getAllNotes}
          onClose={() => {
            setOpenAddEditModal({ isShown: false, type: "add", data: null });
          }}
          showToastMessage={showToastMessage}
        />
      </Modal>

      <button
        className="add-notes-btn"
        onClick={() => {
          setOpenAddEditModal({ isShown: true, type: "add", data: null });
        }}
      >
        <FontAwesomeIcon icon={faPlus} className="addNotes-icon" />
      </button>

      <ToastMessage
        isShown={showToast.isShown}
        message={showToast.message}
        type={showToast.type}
        onClose={handleCloseToast}
      />

      <OffCanvasComponent
        title={isOffCanvas.title}
        content={isOffCanvas.content}
        tags={isOffCanvas.tags}
        createdOn={isOffCanvas.createdOn}
        isOpen={isOpen}
        toggle={toggle}
      />

    </>
  );
};

export default AxiosNotes;
