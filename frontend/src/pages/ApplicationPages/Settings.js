import React, {useState, useEffect} from 'react'
import ProfileInfo from '../../components/ProfileInfo'
import { useNavigate } from 'react-router-dom';
import axiosInstance from '../../utils/axiosInstance';

export function Settings() {
  // get user info
  const [userInfo, setUserInfo] = useState(null);
  const navigate = useNavigate();

  const getUserInfo = async () => {
    try {
      const response = await axiosInstance.get("/auth/get-user");
      if (response.data && response.data.user) {
        setUserInfo(response.data.user);
        console.log(userInfo);
      }
    } catch (error) {
      console.log(error);
      localStorage.clear();
      navigate("/tasks");
    }
  };

  useEffect(() => {
    getUserInfo();
  }, []);
  return (
    <div className='settings-container'>
      <div className='settings-form'>
        <div className='profile-info'>
        <ProfileInfo userInfo={userInfo}/>
        <p>{userInfo?.fullName}</p>
        </div>
        
        <div className='edit-profile'>
          <div className='email-container'>
            <p>Email</p>
            <div className='email-info'>{userInfo?.email}</div>
          </div>
          <div className='edit-user'>
            <button className='profile-btn'>Edit Password</button>
            <button className='profile-btn'>Edit Username</button>
            <button className='profile-btn'>Delete Account</button>
          </div>
        </div>
      </div>
    </div>
  )
}
