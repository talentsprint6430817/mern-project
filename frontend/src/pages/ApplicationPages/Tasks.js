import React from "react";

// * Importing styles
import '../../styles/Tasks.css';

// * Importing components
import AxiosTasks from "../../axios/AxiosTasks";



export function Tasks() {

  return (
    <div className="tasksPage-container">
        <AxiosTasks />
      </div>
  );
}
