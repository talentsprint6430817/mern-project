import React, { useState, useEffect } from "react";
import { Routes, Route, useNavigate } from "react-router-dom";

// Importing Pages
import {
  FocusSession,
  Tasks,
  HabitsTracker,
  Notes,
  Settings,
} from "../indexPages";

// Importing Components
import Sidebar from "../../components/Sidebar";

// Importing Styles
import "../../styles/Application.css";
import axiosInstance from "../../utils/axiosInstance";
import { SettingsProvider } from "../../components/Focus/FocusProvider";

export default function ApplicationRouting() {
  const [userInfo, setUserInfo] = useState(null);
  const navigate = useNavigate();

  const getUserInfo = async () => {
    try {
      const response = await axiosInstance.get("/auth/get-user");
      if (response.data && response.data.user) {
        setUserInfo(response.data.user);
      }
    } catch (error) {
      console.log(error);
      localStorage.clear();
      navigate("/login");
    }
  };

  useEffect(() => {
    getUserInfo();
  }, []);
  return (
    <SettingsProvider>
      <div className="application-container">
        <Sidebar userInfo={userInfo} />
        <div className="page-container">
          <Routes>
            <Route path="/focus" element={<FocusSession />} />
            <Route path="/tasks" element={<Tasks />} />
            <Route path="/habits" element={<HabitsTracker />} />
            <Route path="/notes" element={<Notes />} />
            <Route path="/settings" element={<Settings />} />
          </Routes>
        </div>
      </div>
    </SettingsProvider>
  );
}