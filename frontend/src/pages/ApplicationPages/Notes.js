import React, { useState } from "react";

// * Importing components
import AxiosNotes from "../../axios/AxiosNotes";


// * Importing Styles
import "../../styles/NotesCard.css";

export function Notes() {
  return (
    <>
      <div className="notesPage-container">
        <AxiosNotes />
      </div>
      
    </>
  );
}
