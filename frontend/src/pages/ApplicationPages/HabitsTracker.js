import React from 'react'

// * Importing components
import AxiosHabits from '../../axios/AxiosHabits';

// * Importing styles
import '../../styles/Habits.css';

export function HabitsTracker() {
  return (
    <div className="habitsPage-container">
        <AxiosHabits />
    </div>
  )
}
