import React from "react";
import { Routes, Route, useLocation } from "react-router-dom";


// Importing pages
import { Home, Login, Features, Apps, Contact } from "../indexPages";

// Importing Components
import NavbarComponent from "../../components/Navbar";

export default function LandingRouting() {
  const location = useLocation();
  let isNavbarVisible = location.pathname !== "/login";

  return (
    <>
      <div>
        {isNavbarVisible ? (
          <>
            <NavbarComponent />
            <Routes>
              <Route path="/home" element={<Home />} />
              <Route path="/features" element={<Features />} />
              <Route path="/Apps" element={<Apps />} />
              <Route path="/Help Center" element={<Contact />} />
            </Routes>
          </>
        ) : (
          <Routes>
            <Route path="/login" element={<Login />} />
          </Routes>
        )}
      </div>
    </>
  );
}
