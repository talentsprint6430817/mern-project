require('dotenv').config();

const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');

// routes imports
const taskRoutes = require('./routes/tasks');
const authRoutes = require('./routes/auth');
const notesRoutes = require('./routes/notes');
const habitsRoutes = require('./routes/habits');
const focusRoutes = require('./routes/focus')

// express app
const app = express();

// middleware
app.use(express.json());
app.use(cors());

// routes
app.use('/tasks', taskRoutes);
app.use('/auth', authRoutes);
app.use('/notes', notesRoutes);
app.use('/habits', habitsRoutes);
app.use('/focus', focusRoutes);

// connect to db
main().catch((err) => console.log(err));
async function main() {
  await mongoose.connect(process.env.MONGODB);
  console.log("connected to database...");
}

// listen for requests
app.listen(process.env.PORT, ()=>{
    console.log("server started running on port:4000....")
})

