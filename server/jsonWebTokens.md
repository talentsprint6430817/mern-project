# Json Web Tokens

### It is used to manage authentication between frontend react app and backend api.To tell the frontend that the user has been authenticated and let the frontend keep a track of it.

### It involves making a special token called a json web token.When a user sends a successfull login or signup request, the server handles the request and if it is a valid credentials then it will create a json web token for that user and then send that token back to the client(browser) which we can access in our frontend application.

### The presence of this web token in the browser would signify to the front-end  application that the user is currently logged in or authenticated.

### So we could the presense or absence of this token to do things like protect certain pages or conditionally show different templates in react app

### We also use this token to protect our backend resources. We might restrict access to certain pages endpoints on the api so that only autheticated users can access data from them. The way we do that is by using this json web token and passsing it from the client to the api as a part of request headers. So on the api, if when a request comes in, the server detects that json web token and evaluates it to be valid then we did allow the user the resources.

## A json web token contains:
### `Header` - contains algorithm used for the JWT
### `Payload` - contains non-sensitive user data
### `signature` - used to verify the token by the server