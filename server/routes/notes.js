const express = require('express');
const router = express.Router();

const { authenticateToken } = require('../utilities');
const { createNotes, getNotes, deleteNotes, updateNotes, updatePinNote, searchNote, filterNotes } = require('../controllers/notesController');

// Apply authenticateToken middleware to all routes
router.get('/', authenticateToken, getNotes);
router.post('/', authenticateToken, createNotes);
router.delete('/:id', authenticateToken, deleteNotes);
router.put('/:id', authenticateToken, updateNotes);
router.put('/pinned/:id', authenticateToken, updatePinNote);
router.get('/searchnote', authenticateToken, searchNote);
router.get('/pinned', authenticateToken, filterNotes);

module.exports = router;
