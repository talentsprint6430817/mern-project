const express = require('express');

const router = express.Router();

const { authenticateToken } = require('../utilities');
const {createSession, deleteSession, updateSession, getSessions} = require('../controllers/focusController');

router.get('/', authenticateToken, getSessions)
router.post('/', authenticateToken, createSession)
router.delete('/:id', authenticateToken, deleteSession)
router.put('/:id', authenticateToken, updateSession)

module.exports = router