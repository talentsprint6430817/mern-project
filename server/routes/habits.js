const express = require("express");
const router = express.Router();

const { authenticateToken } = require('../utilities');

const {
  createHabits,
  getHabits,
  deleteHabits,
  updateHabits,
} = require("../controllers/habitsController");

router.get("/", authenticateToken, getHabits);
router.post("/", authenticateToken, createHabits);
router.delete("/:id", authenticateToken, deleteHabits);
router.put("/:id", authenticateToken, updateHabits);

module.exports = router;
