const express = require("express");

const { authenticateToken } = require('../utilities');
const {
createTask,
  getTasks,
  deleteTask,
  updateTask,
  updateComplete,
  filterDueAsc,
  filterDueDesc,
  filterStatus,
  filterPriority
} = require("../controllers/taskController");

const router = express.Router();

router.get("/", authenticateToken, getTasks);

router.post("/", authenticateToken, createTask);

router.delete("/:id", authenticateToken, deleteTask);

router.put("/:id", authenticateToken, updateTask);

router.put("/status/:id", authenticateToken, updateComplete);

router.get('/filterAsc', authenticateToken, filterDueAsc);

router.get('/filterDesc', authenticateToken, filterDueDesc);

router.get('/status', authenticateToken, filterStatus);

router.get('/priority', authenticateToken, filterPriority);

module.exports = router;
