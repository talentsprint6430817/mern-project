const express = require("express");

const router = express.Router();

const { authenticateToken } = require('../utilities');

// controller functions
const {
  loginUser,
  signupUser,
  deleteUser,
  updateUser,
  getUsers,
  updateUserName,
  getUserById
} = require("../controllers/authController");

// login route
router.post("/login", loginUser);

// signup route
router.post("/signup", signupUser);

// delete user route
router.delete("/:id", deleteUser);

// update user password route
router.put("/password/:id", updateUser);

// update user name route
router.put("/username/:id", updateUserName);

// get users route
router.get("/users", getUsers);

// get users route
router.get("/get-user", authenticateToken, getUserById);



module.exports = router;
