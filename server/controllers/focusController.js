const Focus = require('../models/focusModel');
const mongoose = require('mongoose');

// get all focus sessions
const getSessions = async (req, res)=>{
    try{
        const sessions = await Focus.find({ userId: req.user._id }).sort({createdAt : -1});
        res.status(200).json(sessions)
    } catch (error){
        res.status(400).send({ error: error.message })
    }
}

// create a focus session
const createSession = async (req, res)=>{
    const { label, startTime, endTime, duration, date } = req.body

    // add to doc
    try{
        const session = await Focus.create({ 
            userId: req.user._id,
            label, 
            startTime, 
            endTime, 
            duration, 
            date })
        res.status(200).json(session)
    } catch(error){
        res.status(400).json({ error: error.message })
    }
}

// delete a focus session
const deleteSession = async (req, res)=>{
    const { id } = req.params

    if (!mongoose.Types.ObjectId.isValid(id)){
        return res.status(404).send({error : "No such Focus session recorded"})
    }
    const session = await Focus.findOneAndDelete({_id : id, userId: req.user._id,});

    if (!session){
        return res.status(400).json({ error: error.message })
    }
    res.status(200).send({ message: "Deleted successfully..."})
}

// update a focus session
const updateSession = async (req, res)=>{
    const { id } = req.params;
    const { label } = req.body;

    if (!mongoose.Types.ObjectId.isValid(id)){
        return res.status(404).send({error : "No such Focus session recorded"})
    }
    const session = await Focus.findOneAndUpdate({_id : id, userId: req.user._id}, {...req.body});

    if (!session){
        return res.status(400).json({ error: error.message })
    }
    res.status(200).send({ message: "Updated successfully..."})
}

// get sessions for today

module.exports = {createSession, deleteSession, updateSession, getSessions}