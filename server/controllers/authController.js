const Auth = require('../models/authModel');
const jwt = require('jsonwebtoken');
const mongoose = require('mongoose');

// jwt
const createToken = (_id) =>{ // we are passing _id(mongodb) because this _id is going to be in payload
    return jwt.sign({_id}, process.env.SECRET, { expiresIn: '3d' })
}

// Get user by user_id
const getUserById = async (req, res) => {
    try {
        const user = await Auth.findById(req.user._id);
        if (!user) {
            return res.sendStatus(401);
        }
        res.json({ user });
    } catch (error) {
        res.status(400).json({ error: error.message });
    }
};

// get all users
const getUsers = async (req, res)=>{
    try{
        const users = await Auth.find({}).sort({createdOn : -1});
        res.status(200).json(users)
    } catch (error){
        res.status(400).send("Facing error:", error)
    }
}

// login user
const loginUser = async (req, res)=>{
    const {email, password} = req.body

    try{
        const user = await Auth.login(email, password)

        // create a token
        const token = createToken(user._id)

        res.status(200).json({ user, token, message:"Login Successfull"})

    } catch (error){
        res.status(400).json({ error: error.message })
    }
}

// signup user
const signupUser = async (req, res)=>{
    const {fullName, email, password} = req.body

    try{
        const user = await Auth.signup(fullName, email, password)

        // create a token
        const token = createToken(user._id)

        res.status(200).json({ user, token, message:"Registeration Successfull"})

    } catch (error){
        res.status(400).json({ error: error.message })
    }
}

// delete user
const deleteUser = async (req, res) =>{
    const { id } = req.params;

    if (!mongoose.Types.ObjectId.isValid(id)) {
        return res.status(404).json({ error: "No such User found" });
    }

    const user = await Auth.findByIdAndDelete(id);

    if (!user) {
        return res.status(404).json({ error: "No such User found" });
    }

    res.status(200).json({ message: "Deleted successfully..." });
}

// update user password
const updateUser = async (req, res) =>{
    const { id } = req.params;
    const { password } = req.body;

    if (!mongoose.Types.ObjectId.isValid(id)) {
        return res.status(404).json({ error: "No such User found" });
    }

    try{
        const user = await Auth.updatePassword(id, password)
        // create a token
        const token = createToken(user._id)

        res.status(200).json({ message: "Updated successfully...", token });
    } catch (error) {
        res.status(400).json({ error: error.message });
    }

}

// update user password
const updateUserName = async (req, res) =>{
    const { id } = req.params;
    const { fullName } = req.body;

    if (!mongoose.Types.ObjectId.isValid(id)) {
        return res.status(404).json({ error: "No such User found" });
    }

    try{
        const user = await Auth.findByIdAndUpdate(id, { fullName: fullName }, { new: true })
        // create a token
        const token = createToken(user._id)

        res.status(200).json({ message: "Updated successfully...", token });
    } catch (error) {
        res.status(400).json({ error: error.message });
    }

}


module.exports = { loginUser, signupUser, deleteUser, updateUser, getUsers, updateUserName, getUserById }