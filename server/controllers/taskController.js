const Task = require('../models/taskModel')
const mongoose = require('mongoose');

// get all tasks
const getTasks = async (req, res)=>{
    try{
        const tasks = await Task.find({userId: req.user._id}).sort({createdAt : -1});
        res.status(200).json(tasks)
    } catch (error){
        res.status(400).json({ error: error.message })
    }
}

// create new task
const createTask = async (req, res)=>{
    const {title, due, priority, tags} = req.body

    // add to doc
    try{
        const task = await Task.create({userId: req.user._id, title, due, priority, tags});
        res.status(200).json(task)
    }catch(error){
        res.status(400).json({ error: error.message })
    }
}

// delete a task
const deleteTask = async (req, res)=>{
    const { id } = req.params

    if (!mongoose.Types.ObjectId.isValid(id)){
        return res.status(404).send({error : "No such Task"})
    }
    const task = await Task.findOneAndDelete({_id : id, userId: req.user._id});

    if (!task){
        return res.status(400).json({ error: error.message })
    }
    res.status(200).send({ message: "Deleted successfully..."})
        
}

// update a task
const updateTask = async (req, res)=>{
    const { id } = req.params

    if (!mongoose.Types.ObjectId.isValid(id)){
        return res.status(404).send({error : "No such Task"})
    }
    const task = await Task.findOneAndUpdate({_id : id, userId: req.user._id}, {...req.body});

    if (!task){
        return res.status(400).json({ error: error.message })
    }
    res.status(200).send({ message: "Updated successfully..."})
}

// update completed
const updateComplete = async (req, res)=>{
    const { id } = req.params
    const { isCompleted } = req.body

    try{
        const task = await Task.findOne({_id : id});
        if (!task){
            return res.status(404).json({ error: true, message:"No such Task found" });
        }

        task.isCompleted = isCompleted;
        await task.save()
        return res.status(200).json({message: "Task updated successfully"})

     } catch (error){
        res.status(400).json({error: error.message});
     }
}

const filterDueAsc = async (req, res) => {
  try {
    const tasks = await Task.find({
      userId: req.user._id,
    }).sort({ due: 1 });
    return res.status(200).json({ message: "Tasks retrieved successfully", tasks });
  } catch (error) {
    res.status(400).json({ error: error.message });
  }
};

const filterDueDesc = async (req, res) => {
    try {
      const tasks = await Task.find({
        userId: req.user._id,
      }).sort({ due: -1 });
      return res.status(200).json({ message: "Tasks retrieved successfully", tasks });
    } catch (error) {
      res.status(400).json({ error: error.message });
    }
  };

const filterStatus = async (req, res) => {
  const { status } = req.query;
  try {
    const tasks = await Task.find({
      userId: req.user._id,
      isCompleted: status === 'true', // Ensure boolean conversion
    })
    return res.status(200).json({ message: "Tasks retrieved successfully", tasks });
  } catch (error) {
    res.status(400).json({ error: error.message });
  }
};

const filterPriority = async (req, res) => {
  // Define priority mapping
  const priorityMap = {
    low: 1,
    medium: 2,
    high: 3
  };

  try {
    const tasks = await Task.find({ userId: req.user._id });

    tasks.sort((a, b) => {
      return (priorityMap[a.priority] - priorityMap[b.priority]) * (-1);
    });

    return res.status(200).json({ message: "Tasks retrieved successfully", tasks });
  } catch (error) {
    res.status(400).json({ error: error.message });
  }
};


// filter by tags

module.exports = {createTask, getTasks, deleteTask, updateTask, updateComplete, filterDueAsc, filterDueDesc,filterStatus, filterPriority }