const Habits = require('../models/habitsModel');
const mongoose = require('mongoose');

// get all habits
const getHabits = async (req, res)=>{
    try{
        const habits = await Habits.find({userId: req.user._id}).sort({createdAt : -1});
        res.status(200).json(habits)
    } catch (error){
        res.status(400).json({ error: error.message })
    }
}

// create new habit
const createHabits = async (req, res)=>{
    const {title, frequency, reminder, goalDays} = req.body

    // add to doc
    try{
        const habit = await Habits.create({userId: req.user._id, title, frequency, reminder, goalDays});
        res.status(200).json(habit)
    }catch(error){
        res.status(400).json({ error: error.message })
    }
}

// delete a habit
const deleteHabits = async (req, res)=>{
    const { id } = req.params

    if (!mongoose.Types.ObjectId.isValid(id)){
        return res.status(404).send({error : "No such Habit recorded"})
    }
    const habit = await Habits.findOneAndDelete({_id : id, userId: req.user._id});

    if (!habit){
        return res.status(400).json({ error: error.message })
    }
    res.status(200).send({ message: "Deleted successfully..."})
        
}

// update a habit
const updateHabits = async (req, res)=>{
    const { id } = req.params

    if (!mongoose.Types.ObjectId.isValid(id)){
        return res.status(404).send({error : "No such Habit recorded"})
    }
    const habit = await Habits.findOneAndUpdate({_id : id, userId: req.user._id}, {...req.body});

    if (!habit){
        return res.status(400).json({ error: error.message })
    }
    res.status(200).send({ message: "Updated successfully..."})
}

module.exports = {createHabits, getHabits, deleteHabits, updateHabits}