const Notes = require('../models/notesModel');
const mongoose = require('mongoose');


// Get all notes for the authenticated user
const getNotes = async (req, res) => {
    try {
        const notes = await Notes.find({ userId: req.user._id }).sort({ createdOn: -1 });
        res.status(200).json(notes);
    } catch (error) {
        res.status(400).json({ error: error.message });
    }
}

// Create new note
const createNotes = async (req, res) => {
    const { title, content, tags, isPinned } = req.body;

    if (!title) {
        return res.status(400).json({ error: true, message: "Title is required" });
    }

    if (!content) {
        return res.status(400).json({ error: true, message: "Content is required" });
    }

    // Add to document
    try {
        const notes = await Notes.create({
            userId: req.user._id, // Save the user ID with the note
            title,
            content,
            tags: tags || [],
            isPinned
        });
        res.status(200).json({ message: "Note added successfully", notes });
    } catch (error) {
        res.status(400).json({ error: error.message });
    }
}

// Delete a note
const deleteNotes = async (req, res) => {
    const { id } = req.params;

    if (!mongoose.Types.ObjectId.isValid(id)) {
        return res.status(404).send({ error: "No such Notes found" });
    }

    const notes = await Notes.findOneAndDelete({ _id: id, userId: req.user._id });

    if (!notes) {
        return res.status(400).json({ error: error.message });
    }
    res.status(200).send({ message: "Deleted successfully..." });
}

// Update a note
const updateNotes = async (req, res) => {
    const { id } = req.params;
    const { title, content, tags, isPinned } = req.body;

    if (!title && !content && !tags) {
        return res.status(400).json({ error: true, message: "No changes made" });
    }

    try {
        const notes = await Notes.findOne({ _id: id, userId: req.user._id });
        if (!notes) {
            return res.status(404).json({ error: true, message: "No such Notes found" });
        }

        if (title) notes.title = title;
        if (content) notes.content = content;
        if (tags) notes.tags = tags;
        if (isPinned !== undefined) notes.isPinned = isPinned;

        await notes.save();

        return res.status(200).json({ message: "Note updated successfully" });
    } catch (error) {
        res.status(400).json({ error: error.message });
    }
}

// Pin a note
const updatePinNote = async (req, res) => {
    const { id } = req.params;
    const { isPinned } = req.body;

    try {
        const notes = await Notes.findOne({ _id: id, userId: req.user._id });
        if (!notes) {
            return res.status(404).json({ error: true, message: "No such Notes found" });
        }

        notes.isPinned = isPinned;
        await notes.save();
        return res.status(200).json({ message: "Note updated successfully" });
    } catch (error) {
        res.status(400).json({ error: error.message });
    }
}

// Search a note
const searchNote = async (req, res) => {
    const { query } = req.query;

    if (!query) {
        return res.status(400).json({ error: true, message: "Search query is required" });
    }

    try {
        const notes = await Notes.find({
            userId: req.user._id,
            $or: [
                { title: { $regex: new RegExp(query, "i") } },
                { content: { $regex: new RegExp(query, "i") } },
            ],
        });
        return res.status(200).json({ message: "Search query retrieved successfully", notes });
    } catch (error) {
        res.status(400).json({ error: error.message });
    }
}

//  Filter notes - by pinned
const filterNotes = async (req, res)=>{
    try{
        const filtered = await Notes.find({
            userId: req.user._id,
            isPinned : true
        }).sort({ createdOn: -1 });
        return res.status(200).json({message: "Pinned notes retrieved successfully", filtered})
    } catch(error){
        res.status(400).json({ error: error.message });
    }
}

// Filter by tags


module.exports = { createNotes, getNotes, deleteNotes, updateNotes, updatePinNote, searchNote, filterNotes };
