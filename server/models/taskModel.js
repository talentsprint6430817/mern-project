const mongoose = require('mongoose')

// Schema of our todo tasks page database
const Schema = mongoose.Schema

const taskSchema = new Schema({
    userId:{
        type: String,
        required: true
    },
    title: {
        type: String,
        required: true,
        trim: true
    },
    due: {
        type: Date,
        default: new Date()
    },
    priority: {
        type: String,
        emum: ['low', 'medium', 'high'], // This restricts the 'priority' feild to only accept 'low', 'medium', 'high'
        default: 'low'
    },
    tags: {
        type: [String], //This allows to store multiple tags for a single task
        default: []
    },
    isCompleted: {
        type: Boolean,
        default: false
    }
}, {timestamps: true})

module.exports = mongoose.model('Task', taskSchema)

