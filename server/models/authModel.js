const { default: mongoose } = require("mongoose");
const bcrypt = require("bcrypt");
const validator = require("validator");

const Schema = mongoose.Schema;

const authSchema = new Schema({
  fullName:{
    type: String,
    required: true
  },
  email: {
    type: String,
    required: true,
    unique: true,
  },
  password: {
    type: String,
    required: true,
  },
  createdOn: {
    type: Date,
    default: new Date().getTime()
  }
});

// static signup method for hashing the password. We will call this method whenever a user tries to signup(register)
// NOTE : This needs to be a regular function not arrow function because we using "this" keyword.
authSchema.statics.signup = async function (fullName, email, password) {
  // validator - to check if the email enterd is in email format and the password entered is valid
  if (!email || !password || !fullName) {
    throw Error("All fields must be filled");
  }
  if (!validator.isEmail(email)) {
    throw Error("Email is not valid");
  }
  if (!validator.isStrongPassword(password)) {
    throw Error("Password not strong enough");
  }

  const exists = await this.findOne({ email }); // to check if the email already exists

  if (exists) {
    throw Error("Email already in use");
  }

  // hashing the password
  const saltrounds = await bcrypt.genSalt(10);
  const hash = await bcrypt.hash(password, saltrounds);

  const user = await this.create({ fullName, email, password: hash });

  return user;
};

// static login method
authSchema.statics.login = async function (email, password) {
  if (!email || !password) {
    throw Error("All fields must be filled");
  }

  const user = await this.findOne({ email });

  if (!user) {
    throw Error("Incorrect Email");
  }

  const match = await bcrypt.compare(password, user.password);

  if (!match) {
    throw Error("Incorrect password");
  }
  return user;
};

// static update password method
authSchema.statics.updatePassword = async function (id, password) {
  if (!password) {
    throw Error("Password is required to update");
  }

  if (!validator.isStrongPassword(password)) {
    throw Error("Password not strong enough");
  }

  // hashing the password
  const saltrounds = await bcrypt.genSalt(10);
  const hash = await bcrypt.hash(password, saltrounds);

  const user = await this.findByIdAndUpdate(id, { password: hash }, { new: true });

  if(!user){
    throw Error("No user found")
  }

  return user;
};

module.exports = mongoose.model("Auth", authSchema);
