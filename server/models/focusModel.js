const mongoose = require('mongoose');

// schema for our focus sessions page database
const Schema = mongoose.Schema

const focusSchema = new Schema({
    userId:{
        type: String,
        required: true
    },
    label: {
        type: String,
        trim: true,
        default: ''
    },
    startTime: {
        type: String,
        required: true
    },
    endTime: {
        type: String,
        required: true
    },
    duration: {
        type: Number,
        required: true
    },
    date: {
        type: String,
        required:true
    }

}, {timestamps: true})

module.exports = mongoose.model('Focus', focusSchema)