const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const habitsSchema = new Schema({
  userId:{
    type: String,
    required: true
},
  title: {
    type: String,
    trim: true,
    required: true,
  },
  frequency: {
    type: {
      type: String,
      enum: ["daily", "specificDays", "interval"],
    },
    specificDays: {
      type: [String],
      default: undefined,
    },
    intervalDays: {
      type: Number,
      default: undefined,
    },
  },
  reminder: {
    time: {
      type: String,
      enum: [
        null,
        "00:00", "00:30", "01:00", "01:30", "02:00", "02:30",
        "03:00", "03:30", "04:00", "04:30", "05:00", "05:30",
        "06:00", "06:30", "07:00", "07:30", "08:00", "08:30",
        "09:00", "09:30", "10:00", "10:30", "11:00", "11:30",
        "12:00", "12:30", "13:00", "13:30", "14:00", "14:30",
        "15:00", "15:30", "16:00", "16:30", "17:00", "17:30",
        "18:00", "18:30", "19:00", "19:30", "20:00", "20:30",
        "21:00", "21:30", "22:00", "22:30", "23:00", "23:30",
      ],
      default: null,
    },
  },
  goalDays: {
    type: {
      type: String,
      enum: ["Forever", "custom"],
      default: "Forever",
    },
    days: {
      type: Number,
      default: undefined,
    },
  },
  streakDays: {
    type: Number,
    default: 0,
  },
  completed: {
    type: Boolean,
    default: false,
  },
  createdAt: {
    type: Date,
    default: Date.now,
  },
},{timestamps: true});


// Pre-save hook to set default values based on frequency type and goalDays type
habitsSchema.pre('save', function (next) {
  if (!this.frequency.type) {
    if (this.frequency.specificDays && this.frequency.specificDays.length > 0) {
      this.frequency.type = 'specificDays';
    } else if (this.frequency.intervalDays && this.frequency.intervalDays > 0) {
      this.frequency.type = 'interval';
    } else {
      this.frequency.type = 'daily';
    }
  }
  
  if (!this.goalDays.type || this.goalDays.type === 'Forever') {
    if (this.goalDays.days) {
      this.goalDays.type = 'custom';
    } else {
      this.goalDays.type = 'Forever';
    }
  }
  
  next();
});



module.exports = mongoose.model("Habits", habitsSchema);
